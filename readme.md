# OAIPMH Harvester

## About
This a PHP class for harvesting OAI PMH Data Provider.

## How to use it

### Installation
Using composer: 
```bash
composer require fdmi/oaipmhharvester
```

### Load in your code
```php
`require 'vendor/autoload.php';
```

### Set the repo

```php
### REPO LISTS SAMPLE
#Eprints
$base_url = 'https://digilib.uin-suka.ac.id/cgi/oai2';
# Sample with deleted records
# https://digilib.uin-suka.ac.id/cgi/oai2?verb=ListRecords&metadataPrefix=oai_dc&from=2018-04-06T04:00:01Z&until=2018-04-07T04:00:01Z
$base_url = 'http://eprints.ipdn.ac.id/cgi/oai2';
$base_url = 'https://repositori.kemdikbud.go.id/cgi/oai2';
#SLiMS
$base_url = 'http://localhost/demo3/slims9_bulian/oai2.php';
$base_url = 'https://pustaka.kemdikbud.go.id/libdikbud/oai2.php';
#DSPACE
$base_url = 'https://repositori.uma.ac.id/oai/request';
# OPAC UIN SUKA
$base_url = 'https://opac.uin-suka.ac.id/oai-pmh/public/';
# OPAC UIN JAKARTA
$base_url = 'https://tulis.uinjkt.ac.id/oai/provider';
# OJS
$base_url = 'http://jps.ui.ac.id/index.php/jps/oai';
# INLISLITE
$base_url = 'https://pustakamaya.lan.go.id/opac/oai';
$base_url = 'http://inlislite.dispersip.tubankab.go.id/opac/oai';

```

### Creating harvester instance
```php
$harvester = new Fdmi\OaipmhHarvester\Init($base_url);
```
### Getting OAI PMH Profile (verb `Identify`)
- This verb is used to retrieve information about a repository.
- Some of the information returned is required as part of the OAI-PMH.
- Repositories may also employ the Identify verb to return additional descriptive information.

Create Identify instance:
```php
$Identify = new Fdmi\OaipmhHarvester\Identify($harvester->getResource());
```

Dump the data:
```php
var_dump($Identify->getResponse());
```

### Getting List of records (verb `ListRecords`)
- This verb is used to harvest records from a repository
    ```php
    $eparam['metadataPrefix'] = 'oai_dc';
    $ListRecords = new Fdmi\OaipmhHarvester\ListRecords($harvester->getResource(), $eparam);
    ```

Dump the data:

```php
var_dump($ListRecords->getResponse());
```

You can also di selective harvesting based on some criteria:
- `from`. An optional argument with a **UTCdatetime** value. Specify a lower bound for datestamp-based selective harvesting. 
- `until`. An optional argument with a **UTCdatetime** value. Specify a upper bound for datestamp-based selective harvesting. 
- `set` (grouping / category). Optional construct for grouping items. Repositories may organize items into sets. Set organization may be flat, i.e. a simple list, or hierarchical.

    Contoh:
    ```php
    # Set additional parameters
    $eparam['metadataPrefix'] = 'oai_dc';
    $eparam['from'] = '2018-01-01';
    $eparam['until'] = '2018-12-31';
    $eparam['set'] = '7374617475733D707562';
    $ListRecords = new Fdmi\OaipmhHarvester\ListRecords($harvester->getResource(), $eparam);
    var_dump($ListRecords->getResponse());
    ```

- `resumptionToken` (flow control). Just read: http://www.openarchives.org/OAI/openarchivesprotocol.html#FlowControl. In some cases, some repository softwares only allow two parameters: `verb` dan `resumptionToken` when doing request using `resumptionToken` parameter.

    ```php
    # Set additional parameters
    $eparam['resumptionToken'] = 'metadataPrefix%3Doai_dc%26offset%3D209';
    $ListRecords = new Fdmi\OaipmhHarvester\ListRecords($harvester->getResource(), $eparam);
    var_dump($ListRecords->getResponse());
    ```


> **UTCdatetime**. Dates and times are uniformly encoded using ISO8601 and are expressed in UTC throughout the protocol. When time is included, the special UTC designator ("Z") must be used. UTC is implied for dates although no timezone designator is specified. For example, `1957-03-20T20:30:00Z` is UTC 8:30:00 PM on March 20th 1957.
> The legitimate formats are YYYY-MM-DD and YYYY-MM-DDThh:mm:ssZ. Both arguments (`from` and `until`) must have the same granularity.



### Getting List of Identifiers (verb `ListIdentifiers`)
- This verb is an abbreviated form of `ListRecords`, retrieving only headers rather than records.
- Selective harvesting in `ListRecords` can also be applied in `ListIdentifiers`.
    ```php
    # Set additional parameters
    $eparam['from'] = '2018-01-01';
    $eparam['until'] = '2018-12-31';
    $ListIdentifiers = new Fdmi\OaipmhHarvester\ListIdentifiers($harvester->getResource(), $eparam);
    var_dump($ListIdentifiers->getResponse());
    ```


### Getting A Record (verb GetRecord)
- `identifier` and `metadataPrefix` are required.

    ```php
    $eparam['identifier'] = 'oai:digilib.uin-suka.ac.id:4225';
    $eparam['metadataPrefix'] = 'oai_dc';
    # or if you want to see sample of deleted record
    # $eparam['identifier'] = 'oai:digilib.uin-suka.ac.id:15523';
    $GetRecord = new Fdmi\OaipmhHarvester\GetRecord($harvester->getResource(), $eparam);
    var_dump($GetRecord->getResponse());
    ```


### Getting List of Sets (verb `ListSets`)
- This verb is used to retrieve the set structure of a repository, useful for selective harvesting.
- An optional `resumptionToken` can be used for flow control.
    ```php
    $ListSets = new Fdmi\OaipmhHarvester\ListSets($harvester->getResource());
    var_dump($ListSets->getResponse());
    ```

### Getting List of Metadata Formats (verb ListMetadataFormats)
- This verb is used to retrieve the metadata formats available from a repository.
    ```php
    $ListMetadataFormats = new Fdmi\OaipmhHarvester\ListMetadataFormats($harvester->getResource());
    var_dump($ListMetadataFormats->getResponse());
    ```

- You also can add optional `identifier` argument restricts the request to the formats available for a specific item.
    ```php
    $eparam['identifier'] = 'oai:digilib.uin-suka.ac.id:4225';
    $ListMetadataFormats = new Fdmi\OaipmhHarvester\ListMetadataFormats($harvester->getResource(), $eparam);
    var_dump($ListMetadataFormats->getResponse());
    ```
