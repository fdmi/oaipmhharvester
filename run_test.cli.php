<?php
require 'vendor/autoload.php';

$base_url = 'https://digilib.uin-suka.ac.id/cgi/oai2'; # Eprints
#$base_url = 'https://pustaka.kemdikbud.go.id/libdikbud/oai2.php';
#$base_url = 'https://repositori.uma.ac.id/oai/request'; #dspace
#$base_url = 'http://jps.ui.ac.id/index.php/jps/oai'; # OJS
#$base_url = 'http://inlislite.dispersip.tubankab.go.id/opac/oai'; # inlislite
#$base_url = 'https://demo.slims.asia/oai2.php'; #slims 9.6.0

$harvester = new Fdmi\OaipmhHarvester\Init($base_url);

# Identify
$Identify = new Fdmi\OaipmhHarvester\Identify($harvester->getResource());
var_dump($Identify->getResponse());

# ListRecords
#$eparam['metadataPrefix'] = 'oai_dc';
#$eparam['from'] = '2023-01-01';
#$eparam['until'] = '2023-07-29';
#$eparam['set'] = '1984';
#$eparam['resumptionToken'] = '100__+AND+CreateDate+%3E%3D+%272023-01-01%27+AND+CreateDate+%3C%3D+%272023-07-29%27__oai_dc';
#$ListRecords = new Fdmi\OaipmhHarvester\ListRecords($harvester->getResource(), $eparam);
#var_dump($ListRecords->getResponse());

# ListIdentifiers
#$eparam['metadataPrefix'] = 'oai_dc';
#$eparam['from'] = '2000-01-01';
#$eparam['until'] = '2000-01-02';
#$eparam['set'] = '1984';
#$eparam['resumptionToken'] = 'metadataPrefix%3Doai_dc%26offset%3D21759';
#$ListIdentifiers = new Fdmi\OaipmhHarvester\ListIdentifiers($harvester->getResource(), $eparam);
#var_dump($ListIdentifiers->getResponse());

# GetRecord
# oai:digilib.uin-suka.ac.id:4225
# oai:pustaka.kemdikbud.go.id:slims-50040
# oai:repositori.uma.ac.id:123456789/19070
# oai:ojs2.upyodha.com:article/697
# oai:inlislite.dispersip.tubankab.go.id:102
# oai:demo.slims.asia:slims-16
#$eparam['identifier'] = 'oai:demo.slims.asia:slims-16';
#$eparam['metadataPrefix'] = 'oai_dc';
# or if you want to see sample of deleted record
# $eparam['identifier'] = 'oai:digilib.uin-suka.ac.id:15523';
#$GetRecord = new Fdmi\OaipmhHarvester\GetRecord($harvester->getResource(), $eparam);
#var_dump($GetRecord->getResponse());

# ListSet
#$ListSets = new Fdmi\OaipmhHarvester\ListSets($harvester->getResource());
#var_dump($ListSets->getResponse());

# ListMetadataFormats
# All Metadata Formats
#$ListMetadataFormats = new Fdmi\OaipmhHarvester\ListMetadataFormats($harvester->getResource());
# Specific Metadata Format by item identifier
# oai:digilib.uin-suka.ac.id:4225
# oai:pustaka.kemdikbud.go.id:slims-50040
# oai:repositori.uma.ac.id:123456789/19070
# oai:ojs2.upyodha.com:article/697
# oai:inlislite.dispersip.tubankab.go.id:102
# oai:demo.slims.asia:slims-16
#$eparam['identifier'] = 'oai:demo.slims.asia:slims-16';
#$ListMetadataFormats = new Fdmi\OaipmhHarvester\ListMetadataFormats($harvester->getResource(), $eparam);
#var_dump($ListMetadataFormats->getResponse());
