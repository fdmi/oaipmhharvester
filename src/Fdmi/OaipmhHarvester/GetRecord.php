<?php
namespace Fdmi\OaipmhHarvester;

class GetRecord
{
    public $base_url;
    public $oai_url;
    public $param = array ();
    public $response;
    
    public function __construct($base_url, $eparam = NULL)
    {
        $this->base_url = $base_url;
        #$this->param['verb'] = 'ListRecords';
        $this->setParam($eparam);
        # Build the http parameter query
        $this->oai_url = $this->base_url.'?'.http_build_query($this->param);
        #die($this->oai_url."\n");
        $this->setResponse();
    }

    private function setParam($eparam)
    {
        $param = array();
        $v = new \Valitron\Validator($eparam);
        $v->rule('required', ['identifier']);
        # do validation
        if($v->validate()) {
            $param['identifier'] = $eparam['identifier'];
        } else {
            die("Identifier is required.\n");
        }
        $param['verb'] = 'GetRecord';
        $param['metadataPrefix'] = 'oai_dc';
        if (isset($eparam['metadataPrefix'])) {
            # nanti tambahkan verifikasi dengan metdata format list
            $param['metadataPrefix'] = $eparam['metadataPrefix'];
        }
        $this->param = $param;
    }


    private function setResponse()
    {
        # set empty response
        $response = array ();

        # Create xml to array
        $xmlObj = simplexml_load_file($this->oai_url);

        #$response['responseDate'] = (string) $xmlObj->responseDate;
        #var_dump($xmlObj->responseDate); die(); //debug

        $response['request'] = json_decode(json_encode($xmlObj->request), true);
        #var_dump($response); die();

        $xmlArr = json_decode(json_encode($xmlObj), true);
        #var_dump($xmlArr['ListRecords']['resumptionToken']); die();

        if (isset($xmlArr['responseDate'])) {
            $response['responseDate'] = $xmlArr['responseDate'];
        }
        #var_dump($response); die();

        #var_dump($xmlObj);
        #$xmlNode = $xmlObj->GetRecord;
        #foreach ($xmlNode->record as $rNode) {
        #    $metadata = json_decode(json_encode($rNode->metadata->children('oai_dc', 1)->dc->children('dc', 1)), true);
            #var_dump($metadata);
        #    $response['GetRecord']['record']['metadata'] = $metadata;
        #}
        #var_dump($response); die();

        if (isset($xmlArr['GetRecord'])) {
            #die('Data tidak tersedia.');
            $xmlNode = $xmlObj->GetRecord;
            if (count ($xmlArr['GetRecord']) > 0) {
                foreach ($xmlNode->record as $rNode) {
                    $record_header = json_decode(json_encode($rNode->header), true);
                    $response['GetRecord']['record']['header'] = json_decode(json_encode($rNode->header), true);
                    # save this about how to check the record is deleted status or not.
                    #if (isset($response['ListRecords'][$record_counter]['record']['header']['@attributes']['status'])) {
                    #    if ($response['ListRecords'][$record_counter]['record']['header']['@attributes']['status'] == 'deleted') {
                    #        die('deleted record');
                    #    }
                    #}
                    if (!is_null($rNode->metadata->children('oai_dc', 1))) {
                        $metadata = json_decode(json_encode($rNode->metadata->children('oai_dc', 1)->dc->children('dc', 1)), true);
                        $response['GetRecord']['record']['metadata'] = $metadata;
                    }
                    #break;
                }
                #if (isset($xmlArr['GetRecord']['resumptionToken'])) {
                #    $response['GetRecord']['resumptionToken'] = json_decode(json_encode($xmlObj->GetRecord->resumptionToken), true);
                #}
            } else {
                #$response['message'] = 'no data available';
                if (isset($xmlArr['error'])) {
                    $response['error'] = json_decode(json_encode($xmlObj->error), true);;
                }
            }
        } else {
            #die('Data tidak tersedia loh.');
            if (isset($xmlArr['error'])) {
                $response['error'] = json_decode(json_encode($xmlObj->error), true);;
            }
        }
        $this->response = $response;
    }

    public function getResponse()
    {
        return $this->response;
    }

}