<?php
namespace Fdmi\OaipmhHarvester;

class Identify
{
    public $base_url;
    public $oai_url;
    public $param = array ();
    public $response;
    public function __construct($base_url)
    {
        #self::build();
        $this->base_url = $base_url;
        $this->param['verb'] = 'Identify';
        # Build the http parameter query
        $this->oai_url = $this->base_url.'?'.http_build_query($this->param);
        $this->setResponse();

    }

    private function setResponse()
    {
        # set empty response
        $response = array ();

        # Create xml to array
        $xmlObj = simplexml_load_file($this->oai_url);
        $response['request'] = json_decode(json_encode($xmlObj->request), true);
        # Convert recursively object into array
        $xmlArr = json_decode(json_encode($xmlObj), true);
        ##### HEADER ? #####
        if (isset($xmlArr['responseDate'])) {
            $response['responseDate'] = $xmlArr['responseDate'];
        }
        ####################

        ##### The response must include one instance of the following elements: ######
        $repositoryName = FALSE; $baseURL = FALSE; $protocolVersion = FALSE;
        $earliestDatestamp = FALSE; $deletedRecord = FALSE; $granularity = FALSE;
        # repositoryName
        if (isset($xmlArr['Identify']['repositoryName'])) {
            $response['Identify']['repositoryName'] = $xmlArr['Identify']['repositoryName'];
            $repositoryName = TRUE;
        }
        # baseURL
        if (isset($xmlArr['Identify']['baseURL'])) {
            $response['Identify']['baseURL'] = $xmlArr['Identify']['baseURL'];
            $baseURL = TRUE;
        }
        # protocolVersion
        if (isset($xmlArr['Identify']['protocolVersion'])) {
            $response['Identify']['protocolVersion'] = $xmlArr['Identify']['protocolVersion'];
            $protocolVersion = TRUE;
        }
        # earliestDatestamp
        if (isset($xmlArr['Identify']['earliestDatestamp'])) {
            $response['Identify']['earliestDatestamp'] = $xmlArr['Identify']['earliestDatestamp'];
            $earliestDatestamp = TRUE;
        }
        # deletedRecord
        if (isset($xmlArr['Identify']['deletedRecord'])) {
            # possible values: no, transient, persistent
            $deletedRecord_possible_values = array ('no', 'transient', 'persistent');
            if (in_array((string) $xmlArr['Identify']['deletedRecord'], $deletedRecord_possible_values)) {
                $response['Identify']['deletedRecord'] = $xmlArr['Identify']['deletedRecord'];
                $deletedRecord = TRUE;
            }
        }
        # granularity
        if (isset($xmlArr['Identify']['granularity'])) {
            $granularity_possible_values = array ('YYYY-MM-DD', 'YYYY-MM-DDThh:mm:ssZ');
            if (in_array((string) $xmlArr['Identify']['granularity'], $granularity_possible_values)) {
                $response['Identify']['granularity'] = $xmlArr['Identify']['granularity'];
                $granularity = TRUE;
            }
        }
        # Validation
        if (!($repositoryName OR $baseURL OR $protocolVersion OR $earliestDatestamp OR $deletedRecord OR $granularity)) {
            die("\n".'The response must include one instance of the following elements: repositoryName, baseURL, protocolVersion, earliestDatestamp, deletedRecord, granularity'."\n");
        }
        ##############################################################################

        ##### The response must include one or more instances of the following element: adminEmail #####
        $adminEmail = FALSE;
        # adminEmail
        if (isset($xmlArr['Identify']['adminEmail'])) {
            $response['Identify']['adminEmail'] = $xmlArr['Identify']['adminEmail'];
            $adminEmail = TRUE;
        }
        # Validation
        if (!($adminEmail)) {
            die("\n".'The response must include one or more instances of the following element: adminEmail'."\n");
        }
        ################################################################################################

        ##### The response may include multiple instances of the following optional elements: compression, description
        # compression
        if (isset($xmlArr['Identify']['compression'])) {
            $response['Identify']['compression'] = $xmlArr['Identify']['compression'];
        }
        # description
        if (isset($xmlArr['Identify']['description'])) {
            $response['Identify']['description'] = $xmlArr['Identify']['description'];
        }
        ##############################################################################################################
        $this->response = $response;
    }

    public function getResponse()
    {
        return $this->response;
    }

}