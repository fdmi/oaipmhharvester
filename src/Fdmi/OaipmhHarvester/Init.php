<?php
namespace Fdmi\OaipmhHarvester;

class Init
{
    private $base_url;
    public function __construct($base_url)
    {
        # save $base_url to array so it can be checked by valitron
        $vct['base_url'] = $base_url;
        $v = new \Valitron\Validator($vct);
        $v->rule('required', ['base_url']);
        $v->rule('url', ['base_url']);
        # additional validation, only http url is valid
        $v2 = FALSE;
        if (preg_match("/\b(?:(?:https?|http):\/\/)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $base_url)) {
            $v2 = TRUE;
        }
        # do validation
        if(($v->validate()) AND $v2) {
            # assign base_url
            $this->base_url = $base_url;
            # change php stream context to not verify https certificate.
            $this->setStreamContext();
        } else {
            die('$base_url is required and make sure the url format is valid (start with http or https).'."\n");
        }
    }


    public function getResource()
    {
        return $this->base_url;
    }

    private function setStreamContext()
    {
        $context = stream_context_create(array('ssl'=>array(
            'verify_peer' => false
        )));
        libxml_set_streams_context($context);
    }
}