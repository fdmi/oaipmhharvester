<?php
namespace Fdmi\OaipmhHarvester;

class ListIdentifiers
{
    public $base_url;
    public $oai_url;
    public $param = array ();
    public $response;
    
    public function __construct($base_url, $eparam = NULL)
    {
        $this->base_url = $base_url;
        #$this->param['verb'] = 'ListRecords';
        $this->setParam($eparam);
        # Build the http parameter query
        $this->oai_url = $this->base_url.'?'.http_build_query($this->param);
        $this->setResponse();
    }

    private function setParam($eparam)
    {
        $param = array();
        $param['verb'] = 'ListIdentifiers';
        $param['metadataPrefix'] = NULL;
        $param['from'] = NULL;
        $param['until'] = NULL;
        #$param['from'] = '2010-01-01T07:42:14Z'; # contoh
        #$param['until'] = '2010-01-30T07:42:14Z'; # contoh
        $param['set'] = NULL;
        $param['resumptionToken'] = NULL;
        if (isset($eparam['metadataPrefix'])) {
            # nanti tambahkan verifikasi dengan metdata format list
            $param['metadataPrefix'] = $eparam['metadataPrefix'];
        }
        if (isset($eparam['from'])) {
            $param['from'] = $eparam['from'];
        }
        if (isset($eparam['until'])) {
            $param['until'] = $eparam['until'];
        }
        if (isset($eparam['set'])) {
            # nanti tambahkan verifikasi dengan set list
            $param['set'] = $eparam['set'];
        }
        if (isset($eparam['resumptionToken'])) {
            $param['resumptionToken'] = $eparam['resumptionToken'];
        }
        $this->param = $param;
    }


    private function setResponse()
    {
        # set empty response
        $response = array ();

        # Create xml to array
        $xmlObj = simplexml_load_file($this->oai_url);
        #var_dump($xmlObj->request);# die(); //debug

        $response['request'] = json_decode(json_encode($xmlObj->request), true);
        #var_dump($response); die();
        $xmlArr = json_decode(json_encode($xmlObj), true);

        if (isset($xmlArr['responseDate'])) {
            $response['responseDate'] = $xmlArr['responseDate'];
        }
        if (isset($xmlArr['ListIdentifiers']['header'])) {
            if (count ($xmlArr['ListIdentifiers']['header']) > 0) {
                $record_counter = 0;
                foreach ($xmlArr['ListIdentifiers']['header'] as $vdata) {
                    $response['ListIdentifiers'][$record_counter]['header'] = $vdata;
                    $record_counter++;
                }
                if (isset($xmlArr['ListIdentifiers']['resumptionToken'])) {
                    $response['ListIdentifiers']['resumptionToken'] = $xmlArr['ListIdentifiers']['resumptionToken'];
                }
            } else {
                #$response['message'] = 'no data available';
                if (isset($xmlArr['error'])) {
                    $response['error'] = json_decode(json_encode($xmlObj->error), true);;
                }
            }
        } else {
            if (isset($xmlArr['error'])) {
                $response['error'] = json_decode(json_encode($xmlObj->error), true);;
            }
        }
        $this->response = $response;
    }

    public function getResponse()
    {
        return $this->response;
    }

}