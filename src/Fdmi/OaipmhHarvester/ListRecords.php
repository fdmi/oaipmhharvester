<?php
namespace Fdmi\OaipmhHarvester;

class ListRecords
{
    public $base_url;
    public $oai_url;
    public $param = array ();
    public $response;
    
    public function __construct($base_url, $eparam = NULL)
    {
        $this->base_url = $base_url;
        #$this->param['verb'] = 'ListRecords';
        $this->setParam($eparam);
        # Build the http parameter query
        $this->oai_url = $this->base_url.'?'.http_build_query($this->param);
        $this->setResponse();
    }

    private function setParam($eparam)
    {
        $param = array();
        $param['verb'] = 'ListRecords';
        $param['metadataPrefix'] = NULL;
        $param['from'] = NULL;
        $param['until'] = NULL;
        #$param['from'] = '2010-01-01T07:42:14Z'; # contoh
        #$param['until'] = '2010-01-30T07:42:14Z'; # contoh
        $param['set'] = NULL;
        $param['resumptionToken'] = NULL;
        if (isset($eparam['metadataPrefix'])) {
            # nanti tambahkan verifikasi dengan metdata format list
            $param['metadataPrefix'] = $eparam['metadataPrefix'];
        }
        if (isset($eparam['from'])) {
            $param['from'] = $eparam['from'];
        }
        if (isset($eparam['until'])) {
            $param['until'] = $eparam['until'];
        }
        if (isset($eparam['set'])) {
            # nanti tambahkan verifikasi dengan set list
            $param['set'] = $eparam['set'];
        }
        if (isset($eparam['resumptionToken'])) {
            $param['resumptionToken'] = urldecode($eparam['resumptionToken']);
        }
        $this->param = $param;
    }


    private function setResponse()
    {
        # set empty response
        $response = array ();

        # Create xml to array
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );
        $_xmlObj = file_get_contents($this->oai_url, false, stream_context_create($arrContextOptions));
        $_xmlObj = preg_replace('/[\x{fffe}-\x{ffff}]/u', '', $_xmlObj);
        $xmlObj = simplexml_load_string($_xmlObj);
        #$xmlObj = simplexml_load_file($this->oai_url);
        #var_dump($xmlObj->ListRecords->resumptionToken); //debug

        $response['request'] = json_decode(json_encode($xmlObj->request), true);

        $xmlArr = json_decode(json_encode($xmlObj), true);
        #var_dump($xmlArr['ListRecords']['resumptionToken']); die();

        if (isset($xmlArr['responseDate'])) {
            $response['responseDate'] = $xmlArr['responseDate'];
        }
        if (isset($xmlArr['ListRecords'])) {
            #die('Data tidak tersedia.');
            $xmlNode = $xmlObj->ListRecords;
            if (count ($xmlArr['ListRecords']) > 0) {
                $record_counter = 0;
                foreach ($xmlNode->record as $rNode) {
                    $record_header = json_decode(json_encode($rNode->header), true);
                    $response['ListRecords'][$record_counter]['record']['header'] = json_decode(json_encode($rNode->header), true);
                    # save this about how to check the record is deleted status or not.
                    #if (isset($response['ListRecords'][$record_counter]['record']['header']['@attributes']['status'])) {
                    #    if ($response['ListRecords'][$record_counter]['record']['header']['@attributes']['status'] == 'deleted') {
                    #        die('deleted record');
                    #    }
                    #}
                    if (!is_null($rNode->metadata->children('oai_dc', 1))) {
                        $metadata = json_decode(json_encode($rNode->metadata->children('oai_dc', 1)->dc->children('dc', 1)), true);
                        $response['ListRecords'][$record_counter]['record']['metadata'] = $metadata;
                    }
                    $record_counter++;
                    #break;
                }
                if (isset($xmlArr['ListRecords']['resumptionToken'])) {
                    $response['ListRecords']['resumptionToken'] = json_decode(json_encode($xmlObj->ListRecords->resumptionToken), true);
                }
            } else {
                #$response['message'] = 'no data available';
                if (isset($xmlArr['error'])) {
                    $response['error'] = json_decode(json_encode($xmlObj->error), true);;
                }
            }
        } else {
            #die('Data tidak tersedia loh.');
            if (isset($xmlArr['error'])) {
                $response['error'] = json_decode(json_encode($xmlObj->error), true);;
            }
        }
        $this->response = $response;
    }

    public function getResponse()
    {
        return $this->response;
    }

}