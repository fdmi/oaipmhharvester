<?php
namespace Fdmi\OaipmhHarvester;

class ListSets
{
    public $base_url;
    public $oai_url;
    public $param = array ();
    public $response;
    
    public function __construct($base_url, $eparam = NULL)
    {
        $this->base_url = $base_url;
        #$this->param['verb'] = 'ListRecords';
        $this->setParam($eparam);
        # Build the http parameter query
        $this->oai_url = $this->base_url.'?'.http_build_query($this->param);
        $this->setResponse();
    }

    private function setParam($eparam)
    {
        $param = array();
        $param['verb'] = 'ListSets';
        $param['identifier'] = NULL;
        if (isset($eparam['identifier'])) {
            $v = new \Valitron\Validator($eparam);
            $v->rule('required', ['identifier']);
            if ($v->validate()) {
                $param['identifier'] = $eparam['identifier'];
            } else {
                die("Identifier must be set clearly.");
            }
        }
        $this->param = $param;
    }


    private function setResponse()
    {
        # set empty response
        $response = array ();

        # Create xml object
        $xmlObj = simplexml_load_file($this->oai_url);
        # create the xml array version
        $xmlArr = json_decode(json_encode($xmlObj), true);
        # Get the responseDate. Sample: <responseDate>2023-04-22T23:42:52Z</responseDate>
        if (isset($xmlArr['responseDate'])) {
            $response['responseDate'] = $xmlArr['responseDate'];
        }
        # Get the request info, including the property.
        # Sample: <request verb="ListSets">https://digilib.uin-suka.ac.id/cgi/oai2</request>
        $response['request'] = json_decode(json_encode($xmlObj->request), true);

        if (isset($xmlArr['ListSets'])) {
            $xmlNode = $xmlObj->ListSets;
            $response['ListSets'] = json_decode(json_encode($xmlObj->ListSets), true);
            #if (count ($xmlArr['ListSets']) > 0) {
            #    $record_counter = 0;
            #    foreach ($xmlNode->set as $rNode) {
            #        var_dump($rNode);
            #    }
            #} else {
            #    if (isset($xmlArr['error'])) {
            #        $response['error'] = json_decode(json_encode($xmlObj->error), true);;
            #    }                
            #}
        } else {
            if (isset($xmlArr['error'])) {
                $response['error'] = json_decode(json_encode($xmlObj->error), true);;
            }
        }
        #var_dump($response); //debug
        #die();
    
        $this->response = $response;
    }

    public function getResponse()
    {
        return $this->response;
    }

}